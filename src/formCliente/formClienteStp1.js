import React,{useState,useRef} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

export default function FormCLienteStp1(props) {


  
  
  /**
   * PARA LOS CHECKBOXES
   */

   
  var [letras, setLetras] = useState(false);
  var [numeros, setNumeros] = useState(false);
  var [civilWar, setcivilWar] = useState(false);
  var [zodiaco, setZodiacos] = useState(false);
/*
  useEffect(()=>{

  },[props]);*/

  useRef(()=>{
    setLetras(letras=true);
    setNumeros(numeros=false);
    setcivilWar(civilWar=false);
    setZodiacos(zodiaco=false);
  },[props]);

  var handleLetras = (event)=>{
      if(!letras){
        setLetras(letras=true);
        setFalse("letras");
      }else{
        setLetras(letras=false);
      }
  }

  var handleZodiaco = (event)=>{
      if(!zodiaco){
        setZodiacos(zodiaco=true);
        setFalse("zodiaco");
      }else{
        setZodiacos(zodiaco=false);
      }
  }

  var handleCivilWar = (event)=>{
    if(!civilWar){
      setcivilWar(civilWar=true);
      setFalse("civilWar");
    }else{
      setcivilWar(civilWar=false);
    }
}

  var setFalse=(variable)=>{
    switch (variable) {
      case "letras":
        setcivilWar(civilWar=false);
        setNumeros(numeros=false);
        setZodiacos(zodiaco=false); 
        break;
      case "numeros":
        setcivilWar(civilWar=false);
        setLetras(letras=false);
        setZodiacos(zodiaco=false);
        break;
      case "civilWar":
        setNumeros(numeros=false);
        setLetras(letras=false);
        setZodiacos(zodiaco=false);
        break;
      case "zodiaco":
        setNumeros(numeros=false);
        setLetras(letras=false);
        setcivilWar(civilWar=false);
        break;
    
      default:
        break;
    }
  }

/**
 * END #################################################################===================>>>>>>>>>>>>>>>>>>
 */
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Informacion del cliente
      </Typography>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="firstName"
            name="firstName"
            label="Ingrese su primer nombre mi pana"
            fullWidth
            autoComplete="given-name"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="lastName"
            name="lastName"
            label="su apellido mi pana"
            fullWidth
            autoComplete="family-name"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="address"
            name="address"
            label="Si tiene donde vivir aqui abajo"
            fullWidth
            autoComplete="shipping address-line1"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="city"
            name="city"
            label="Ciudad"
            fullWidth
            autoComplete="shipping address-level2"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField id="state" name="municipio" label="El canton donde vive" fullWidth />
        </Grid>
        <Grid item xs={12} >
        <Typography variant="h6" gutterBottom>
        Marca de los avengers de su huasipungo
      </Typography>
    <div>
      <FormControlLabel
        control={<Checkbox disabled inputProps={{ 'aria-label': 'disabled unchecked checkbox' }} />}
        value={numeros}
        label="Los numeros"
      />
      <FormControlLabel
        control={<Checkbox
          checked={letras}
          size="small"
          inputProps={{ 'aria-label': 'checkbox with small size' }}
          onClick={(event)=>handleLetras(event)}
        />}
        label="Las letrotas"
      />
      <FormControlLabel
        control={<Checkbox
          checked={zodiaco}
          size="small"
          inputProps={{ 'aria-label': 'checkbox with small size' }}
          onClick={(event)=>handleZodiaco(event)}
        />}
        label="Cabelleros del zodiaco"
      />
      <FormControlLabel
      control={
      <Checkbox
        value={civilWar}
        checked={civilWar}
        size="small"
        inputProps={{ 'aria-label': 'checkbox with small size' }}
        onClick={(event)=>handleCivilWar(event)}
        
      />}
      label="No sabe / no se deciden , se encuentran en civil war"
    />
    </div>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
